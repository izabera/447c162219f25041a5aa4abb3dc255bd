*context: shells want to leave the seek position at the right offset so external commands can continue reading from the same fd*

---

i saw this somewhat surprising tally of syscalls in bash. let's investigate

![surprise](https://gist.github.com/user-attachments/assets/7434d301-c3b1-480e-83c6-a3147076c675)

one iteration:
- unblock all signals
- check if the fd is a tty
- check if the fd is seekable
- read 4k
- reposition seek position after the end of the line

then
- unblock all signals again for good measure
- if the fd is now magically a tty?
- maybe it's now seekable?
- read 4k
- seek to next line

and then
- better unblock those signals again
- what if the fd had become a tty tho?
- i really better check if it's seekable again
- read
- seek

and then...

![iteration](https://gist.github.com/user-attachments/assets/3ecfffbc-ed48-4edb-b389-c51f0f9c11bb)

dash doesn't like seeking back so it just reads one byte at a time

![dash](https://gist.github.com/user-attachments/assets/c37cc600-6613-44f1-a946-8f9fc4ddff6b)

i've discovered the 70% rule: shells must do something stupid 70% of the time.  it's probably a fundamental principle of thermodynamics or something

dash wastes 70% of the total runtime purely due to syscall overhead

somehow bash manages to do ??? in userspace instead

![70% rule](https://gist.github.com/user-attachments/assets/f2f72238-8c5d-485a-8b88-0d8b026c0c4e)

mksh is back to reading one byte at a time

![mksh](https://gist.github.com/user-attachments/assets/d93c8026-3f19-45e7-8540-82737acdd051)

zsh also reads one byte at a time, but it _really_ wants to make sure that the signals don't do anything funny

block sigchld and sigwinch.  ok now re block sigchld.  unblock it.  block it again.  block it again again.  unblock it.  block it again.  unblock it.   read

![zsh](https://gist.github.com/user-attachments/assets/8ba981e1-e473-4161-9a12-f36daf3ebbeb)

and both end up slower than bash

![slow](https://gist.github.com/user-attachments/assets/fbc167d3-ebbf-4df0-ab1b-5a2237dd7c46)

surprisingly low numbers from ksh93u+m

![ksh](https://gist.github.com/user-attachments/assets/3a026c39-6595-42d4-bce4-ccdfd1b669a5)

turns out it reads 64k at a time and... doesn't do anything too stupid?!
ok it's still a bit stupid for all the wasted lseeks, but the total syscall overhead was 0.5% so it gets a pass.
good job ksh

![ksh strace](https://gist.github.com/user-attachments/assets/df8a8e65-61de-4929-ba5f-bab8c4f7ed52)

python, a notoriously fast programming language, for comparison

![python](https://gist.github.com/user-attachments/assets/d0b3e067-6ace-45fa-8387-c3147e4fd2b9)

---

next:
[revolutionary invention](https://gist.github.com/izabera/bd37b1ad1f2cc5f2f9c0ad3b35ec5710)

previous:
[short bash quiz](https://gist.github.com/izabera/7dcece1d6df9063d43d2b3b0a222e7bb)